#include <array>
#include <map>
#include <iostream>
#include <sstream>
#include <bitset>
#include <jnes_cpu.h>
#include <jnes_ppu.h>
#define CPU_DEBUG
#ifdef CPU_DEBUG
#include <cpu_debug.h>
#endif /* cpu_debug */

using namespace jnes::cpu;


registers r;

namespace jnes
{
    namespace cpu
    {
        std::array<uint8_t, 0x10000> memory_map;
        std::map<uint8_t, instruction> instructions;


        constexpr uint8_t negative_flag =           0b10000000;
        constexpr uint8_t overflow_flag =           0b01000000;
        constexpr uint8_t break_command_flag =      0b00010000;
        constexpr uint8_t decimal_mode_flag =       0b00001000; //useless. 2A03 doesn't support BCD format.
        constexpr uint8_t interrupt_disable_flag =  0b00000100;
        constexpr uint8_t zero_flag =               0b00000010;
        constexpr uint8_t carry_flag =              0b00000001;

        constexpr uint8_t sign_bit =                negative_flag;

        constexpr uint8_t one_cycle =               1;
        constexpr uint8_t two_cycles =              2;
        constexpr uint8_t three_cycles =            3;
        constexpr uint8_t four_cycles =             4;
        constexpr uint8_t five_cycles =             5;
        constexpr uint8_t six_cycles =              6;
        constexpr uint8_t seven_cycles =            7;
        constexpr uint8_t eight_cycles =            8;
        constexpr uint8_t nine_cycles =             9;

        constexpr uint8_t zero =                    0;


        /*
        * the weakness in this programming way is that i have no way to note when a instruction
        * is in accumulate, inplied, zero page mode, etc. So. In accumulator mode i pass this value
        * because the processor doesn't address this value.
        */
        const uint16_t max_value_16 = -1;

        /*
        * There could be several useless names. But just in case if i can do something
        * special with those names.
        */
        enum struct io_port : uint16_t
        {
            ppu_control                                 = 0x2000, // w
            ppu_mask                                    = 0x2001, // w
            ppu_status_register                         = 0x2002, // r
            ppu_oam_address                             = 0x2003, // w
            ppu_oam_data                                = 0x2004, // w
            ppu_vram_register_1                         = 0x2005, // w
            ppu_vram_register_2                         = 0x2006, // w
            ppu_vram_io_register                        = 0x2007, // r/w
            apu_pulse_1_control_register                = 0x4000, // w
            apu_pulse_1_ramp_control_register           = 0x4001, // w
            apu_pulse_1_fine_tune_register              = 0x4002, // w
            apu_pulse_1_coarse_tune_register            = 0x4003, // w
            apu_pulse_2_control_register                = 0x4004, // w
            apu_pulse_2_ramp_control_register           = 0x4005, // w
            apu_pulse_2_fine_tune_register              = 0x4006, // w
            apu_pulse_2_coarse_tune_register            = 0x4007, // w
            apu_triangle_control_1                      = 0x4008, // w
            apu_triangle_control_2                      = 0x4009, // w
            apu_triangle_frequency_register_1           = 0x400A, // w
            apu_triangle_frequency_register_2           = 0x400B, // w
            apu_noise_control_register_1                = 0x400C, // w
            apu_noise_frequency_register_1              = 0x400E, // w
            apu_noise_frequency_register_2              = 0x400F, // w
            apu_delta_modulation_control_register       = 0x4010, // w
            apu_delta_modulation_da_register            = 0x4011, // w
            apu_delta_modulation_address_register       = 0x4012, // w
            apu_delta_modulation_data_length_register   = 0x4013, // w
            sprite_dma_register                         = 0x4014, // dma
            apu_sound_vertical_clock_signal_register    = 0x4015, // r-w
            joypad_1                                    = 0x4016, // r-w
            joypad_2                                    = 0x4017, // r-w
        };

        inline void increase_pc (const uint8_t n)
        {
            r.PC += n;
        }


        /*
        * SP starts from 0x0100 and ends in 0x01FF; besides, it works as top-down.
        * When you push a value, it is stored and SP decrease one. And, when you pull(pop),
        * SP decrease one and retrieva te value.
        *
        * As SP is uint8_t, there is no way i can reach some value greather than 0x00FF,
        * but i want to keep the variable type because it will wraparound by default.
        * The solution here is to set up a "stack_base" and the SP direction will be
        * stack_base + SP.
        */

        constexpr uint16_t stack_base = 0x0100;
        long long unsigned int push_counter = 0;
        long long unsigned int pull_counter = 0;

        inline void push_stack (const uint8_t data) // helper
        {
            // std::cout << "----------------------------- Push Stack--------------------------------\n";
            // std::cout << re << "\n";
            // uint8_t key = memory_map[r.PC];
            // std::cout << instructions[key].name << " push into stack\n";
            // std::cout << "Push data: " << (int)data << " into: " << int(stack_base + re.SP) << "\n";
            write_memory (stack_base + r.SP, data);
            --r.SP;
            // push_counter++;
        }

        uint8_t pull_stack () // helper
        {
            // std::cout << "-----------------------------Pull Stack--------------------------------\n";
            // uint8_t key = memory_map[r.PC];
            // std::cout << instructions[key].name << " pull from stack\n";
            // std::cout << " pull data: " << (int)read_memory(stack_base + re.PC + 1) << " from: " << int(stack_base + re.SP - 1) << "\n";
            // std::cout << re << "\n";
            ++r.SP;
            const uint8_t d = read_memory (stack_base + r.SP);
            
            // pull_counter++;
            return d;
        }

        /*
        * If you are asking why i those function doesn't return the address. It's because
        * the first function i wrote was template<typename T> inline T get_address_absolute_indexed.
        * i needed to return two values, so i decide to return extra cycles and the address will be passed
        * by reference. So... To keep my methods consistent, i must obtain the address in this way.
        */

        // inline const void get_address_absolute (uint16_t& address, const registers& re)
        // {
        //     address = static_cast<uint16_t>(read_memory (re.PC + 1)) + (static_cast<uint16_t>(read_memory (re.PC + 2)) << 8);
        // }


        // /*
        // * If there is a page boundary crossed. It'll return an extra cycle.
        // * This works for absolute indexed x, absolute indexed y.
        // * In fact, i could use it for absolute addressing mode, but the name make noise for me.
        // */
        // template<typename T>
        // inline T get_address_absolute_indexed (uint16_t& address, const registers& re, const uint8_t indexer);

        // template<>
        // inline uint8_t get_address_absolute_indexed<uint8_t> (uint16_t& address, const registers& re, const uint8_t indexer)
        // {
        //     const uint16_t lsb = static_cast<uint16_t>(read_memory(re.PC + 1));
        //     const uint16_t msb = static_cast<uint16_t>(read_memory(re.PC + 2)) << 8;
        //     address = lsb + msb + indexer;
        //     if (lsb + indexer < indexer) return one_cycle;
        //     else return zero;
        // }

        // template<>
        // inline void get_address_absolute_indexed<void> (uint16_t& address, const registers& re, const uint8_t indexer)
        // {
        //     const uint16_t lsb = static_cast<uint16_t>(read_memory(re.PC + 1));
        //     const uint16_t msb = static_cast<uint16_t>(read_memory(re.PC + 2)) << 8;
        //     address = lsb + msb + indexer;
        // }

        // inline void get_address_zero_page_indexed (uint16_t& address, const registers& re, const uint8_t index)
        // {
        //     address = read_memory (re.PC + 1 + index);
        // }

        // inline void get_address_zero_page (uint16_t& address, const registers& re)
        // {
        //     address = read_memory (re.PC + 1);
        // }

        // inline void get_address_indirect (uint16_t& address, const registers& re)
        // {
        //     get_address_absolute (address, re);
        //     address = static_cast<uint16_t>(read_memory(address)) + 
        //                 (static_cast<uint16_t>(read_memory(address + 1)) << 8);
        // }

        // inline void get_address_zero_page_indexed_indirect (uint16_t& address, const registers& re)
        // {
        //     const uint16_t index = read_memory(re.PC + 1) + re.X;
        //     const uint16_t lsb = static_cast<uint16_t>(read_memory(index));
        //     const uint16_t msb = static_cast<uint16_t>(read_memory(index + 1)) << 8;

        //     address = lsb + msb;
        // }

        // inline void get_address_relative (uint16_t& address, const registers& re)
        // {
        //     address = re.PC;
        // }

        // //Long name. i know. But i think it is necessary to make things clearly althought it seems verbose.
        // inline uint8_t get_address_zero_page_indirect_indexed_y (uint16_t& address, const registers& re)
        // {
        //     const uint8_t index = read_memory (re.PC + 1);
        //     const uint16_t lsb = static_cast<uint16_t>(read_memory(index));
        //     const uint16_t msb = static_cast<uint16_t>(read_memory(index + 1)) << 8;
        //     address = lsb + msb + re.Y;

        //     if (lsb + re.Y < re.Y) return one_cycle;
        //     else return zero;
        // }

        // void flags (const uint8_t f, registers& re, const uint8_t operand)
        // {
        //     if (f & zero_flag)      re.P = (operand == zero) ? re.P | zero_flag : re.P & ~zero_flag;
        //     if (f & negative_flag)  re.P = operand > 0x7F ? re.P | negative_flag : re.P & ~negative_flag;
        // }

        // // OK
        // // register A plus value in memory plus carry.
        // inline void adc (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     const uint8_t operand = re.A + read_memory (address) + (re.P & carry_flag ? 1 : 0);
        //     flags (zero_flag | negative_flag, re, operand);
        //     re.P = ((operand | re.A) > 0x7F) ? re.P | overflow_flag : re.P & ~overflow_flag;
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // // register A minus value in memory minus negate of carry
        // inline void sbc (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     const uint8_t operand = re.A - read_memory (address) - (re.P & carry_flag ? 0 : 1);
        //     flags (zero_flag | negative_flag, re, operand);
        //     re.P = ((operand | re.A) > 0x7F) ? re.P | overflow_flag : re.P & ~overflow_flag;
        //     re.A = operand;
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void lda (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.A = read_memory(address);
        //     // std::cout<<"lda re.A: "<<(int)re.A<<"\n";
        //     flags(zero_flag | negative_flag, re, re.A);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void ldx (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.X =  read_memory(address);
        //     flags(zero_flag | negative_flag, re, re.X);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void ldy (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.Y = read_memory(address);
        //     flags(zero_flag | negative_flag, re, re.Y);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void anda (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.A &= read_memory(address);
        //     flags (zero_flag | negative_flag, re, re.A);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void ora (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
            
        //     re.A |= read_memory(address);
        //     flags (zero_flag | negative_flag, re, re.A);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void eor (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
            
        //     re.A ^= read_memory(address);
        //     flags (zero_flag | negative_flag, re, re.A);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void inc (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     uint8_t operand = read_memory(address);
        //     ++operand;
        //     write_memory(address, operand);
        //     flags (zero_flag | negative_flag, re, operand);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void dec (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     uint8_t operand = read_memory(address);
        //     --operand;
        //     write_memory(address, operand);
        //     flags (zero_flag | negative_flag, re, operand);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void rol (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     uint8_t operand;
        //     uint8_t highest_bit;
        //     auto f = [re, &highest_bit] (uint8_t o) 
        //     {
        //         const uint8_t carry_value = re.P & carry_flag;
        //         highest_bit = o & sign_bit >> 7;
        //         o <<= 1;
        //         return o |= carry_value;
        //     };

        //     if (address == max_value_16)
        //     {
        //         operand = re.A;
        //         operand = f(operand);
        //         re.A = operand;
        //     }
        //     else
        //     {
        //         operand = read_memory (address);
        //         operand = f(operand);
        //         write_memory(address, operand);
        //     }
            
        //     flags (zero_flag | negative_flag, re, operand);
        //     re.P = highest_bit == zero ? re.P & ~carry_flag : re.P | carry_flag;
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void ror (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     uint8_t operand;
        //     uint8_t lowest_bit;
        //     auto f = [re, &lowest_bit] (uint8_t o) 
        //     {
        //         const uint8_t carry_value = re.P & carry_flag;
        //         lowest_bit = o & 0x01;
        //         o >>= 1;
        //         return o |= (carry_value << 7);
        //     };

        //     if (address == max_value_16)
        //     {
        //         operand = re.A;
        //         operand = f(operand);
        //         re.A = operand;
        //     }
        //     else
        //     {
        //         operand = read_memory (address);
        //         operand = f(operand);
        //         write_memory(address, operand);
        //     }

        //     re.P = lowest_bit == zero ? re.P & ~carry_flag : re.P | carry_flag;
        //     increase_pc (pc_increaser);
        // }

        // //OK
        // inline void asl (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     uint8_t value;
        //     if (address == max_value_16)
        //         value = re.A;
        //     else
        //         value = memory_map[address];
        //     re.P = value & 0b10000000 ? re.P | carry_flag : re.P & ~carry_flag;
        //     value <<= 1;
        //     flags (negative_flag | zero_flag, re, value);
        //     if (address == max_value_16)
        //         re.A = value;
        //     else
        //         write_memory (address, value);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void clc (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.P &= ~carry_flag;
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void sec (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.P |= carry_flag;
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void cli (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.P &= ~interrupt_disable_flag;
        //     increase_pc (pc_increaser);
        // }


        // // OK
        // inline void sei (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.P |= interrupt_disable_flag;
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void clv (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.P &= ~overflow_flag;
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void cld (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.P &= ~decimal_mode_flag;
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void sed (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.P |= decimal_mode_flag;
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void nop (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     // Nothing! It is beautiful
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void bit (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     const uint8_t operand = read_memory (address);
        //     re.P = (operand & 0b10000000 ? re.P | negative_flag : re.P & ~negative_flag);
        //     re.P = (operand & 0b01000000 ? re.P | overflow_flag : re.P & ~overflow_flag);
        //     re.P = (operand == zero ? re.P | zero_flag : re.P & ~zero_flag);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void sta (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     write_memory (address, re.A);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void stx (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     write_memory (address, re.X);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void sty (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     write_memory (address, re.Y);
        //     increase_pc (pc_increaser);
        // }

        // /*
        // * jmp is really special. 
        // */
        // template <typename T> 
        // inline void jmp (const uint16_t address, registers& re, const uint8_t pc_increaser);

        // // OK
        // template <>
        // inline void jmp<void> (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.PC = address;
        // }

        // // OK
        // template <>
        // inline void jmp<uint8_t> (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     const uint16_t address2 = static_cast<uint16_t>(jnes::cpu::memory_map[address]) +
        //                                 (static_cast<uint16_t>(jnes::cpu::memory_map[address + 1]) << 8);
        //     re.PC = address2;
        // }

        // // You don't deserve a "normal" definition.
        // auto cmp_flags =  [](int8_t r, registers& re)
        // {
        //     if ( r < zero ) 
        //     {
        //         re.P &=  ~zero_flag & ~carry_flag; 
        //         re.P = (r & negative_flag) ? re.P | negative_flag : re.P & ~negative_flag;
        //     }
        //     if ( !r ) { re.P = (re.P & ~negative_flag) | zero_flag | carry_flag; }
        //     if ( r > zero ) 
        //     {
        //         re.P = ( re.P & ~zero_flag) | carry_flag;
        //         re.P = (r & negative_flag) ? re.P | negative_flag : re.P & ~negative_flag;
        //     }
        // };

        // // OK
        // inline void cmp (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     std::cout << "PC value: " << re.PC << ", pc_increaser: " << (int)pc_increaser << std::endl;
        //     cmp_flags (re.A - read_memory (address), re);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void cpx (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     cmp_flags (re.X - read_memory (address), re);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void cpy (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     cmp_flags (re.Y - read_memory (address), re);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void pha (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     push_stack(re.A, re);
        //     increase_pc (1);
        // }

        // // OK
        // inline void pla (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.A = pull_stack (re);
        //     increase_pc (1);
        // }

        // // OK
        // inline void php (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     push_stack(re.P, re);
        //     increase_pc (1);
        // }

        // // OK
        // inline void plp (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.P = pull_stack (re);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void txs (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.SP = re.X;
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void tsx (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.X = re.SP;
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void tax (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.X = re.A;
        //     flags (zero_flag | negative_flag, re, re.X);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void txa (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.A = re.X;
        //     flags (zero_flag | negative_flag, re, re.A);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void tay (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.Y = re.A;
        //     flags (zero_flag | negative_flag, re, re.Y);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void tya (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.A = re.Y;
        //     flags (zero_flag | negative_flag, re, re.A);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void inx (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     ++re.X;
        //     flags (zero_flag | negative_flag, re, re.X);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void iny (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     ++re.Y;
        //     flags (zero_flag | negative_flag, re, re.Y);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void dex (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     --re.X;
        //     flags (zero_flag | negative_flag, re, re.X);
        //     increase_pc (pc_increaser);
        // }

        // // OK
        // inline void dey (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     --re.Y;
        //     flags (zero_flag | negative_flag, re, re.Y);
        //     increase_pc (pc_increaser);
        // }

        // inline void lsr (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     uint8_t operand;

        //     auto i  = [&re](const uint8_t o)
        //     {
        //         re.P = (o & 0x01) ? re.P | carry_flag : re.P & ~carry_flag;
        //         return o >> 1;
        //     };

        //     if (address == max_value_16)
        //     {
        //         operand = re.A;
        //         re.A = i (operand);
        //     }
        //     else
        //     {
        //         operand = read_memory (address);
        //         write_memory (address, i (operand));

        //     }
        //     increase_pc (pc_increaser);
        // }

        // inline void jsr (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     uint16_t next_instruction = pc_increaser + re.PC;
        //     // increase_pc (pc_increaser);
        //     const uint8_t l = static_cast<uint8_t>(next_instruction & 0xFF);
        //     const uint8_t h = static_cast<uint8_t>((next_instruction >> 8) & 0xFF);
        //     // std::cout << "h: " << (int)h << ", l: " << (int)l << "\n";
        //     if (l == 0x00) { push_stack (h - 1, re); } // A page boundary ocurred.
        //     push_stack(h, re);
        //     push_stack(l - 1, re);
        //     // std::cout <<"stack address: " << stack_base + re.SP << "\n";
        //     re.PC = address;
        //     // std::cout<<"jsr address: "<<re.PC<<"\n";
        // }

        // inline void rts (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     const uint16_t a = static_cast<uint16_t>(pull_stack(re)) +
        //                         (static_cast<uint8_t>(pull_stack (re)) << 8) + 1;
            
        //     re.PC = a;
        // }

        // inline void rti (const uint16_t address, registers& re, const uint8_t pc_increaser)
        // {
        //     re.P = pull_stack(re);
        //     const uint16_t a = static_cast<uint16_t>(pull_stack(re)) +
        //                         (static_cast<uint16_t>(pull_stack (re)) << 8);
        //     re.PC = a;
        // }

        // /*
        // * This method is called by ppu.
        // */
        void nmi_interruption ()
        {
            push_stack (static_cast<uint8_t>(r.PC &0xFF));
            push_stack (static_cast<uint8_t>((r.PC >> 8) & 0xFF));
            push_stack (r.P);
            r.PC = static_cast<uint16_t>(read_memory (0xFFFA)) + (static_cast<uint16_t>(read_memory (0xFFFB)) << 8);
        }



        // /*
        // * Each function_base::base is void type. So it cannot return any value. But in branch instruction,
        // * i didn't know it produce extra cycles. So... Here is the solution. A little trick.
        // */

        // uint8_t extra_cycles_branch = 0;

        // inline uint8_t get_extra_cycles_branch ()
        // {
        //     const uint8_t e = extra_cycles_branch;
        //     return (extra_cycles_branch = 0, e);
        // }

        // auto branch_base = [](registers& re)
        // {
        //         extra_cycles_branch = 1;
        //         const uint8_t destiny = read_memory (re.PC + 1);
        //         increase_pc (2);
        //         const uint8_t low_pc = static_cast<uint8_t>(re.PC & 0x00FF);
        //         if (low_pc + destiny < destiny) extra_cycles_branch = 2;
        //         re.PC += static_cast<int8_t>(destiny);
        // };

        // inline void bpl (const uint16_t address, registers& re, uint8_t pc_increaser)
        // {
        //     if (!(re.P & negative_flag)) { branch_base (re); return; }
        //     increase_pc (pc_increaser);
        // }

        // inline void bmi (const uint16_t address, registers& re, uint8_t pc_increaser)
        // {
        //     if (re.P & negative_flag) { branch_base (re); return; }
        //     increase_pc (pc_increaser);
        // }

        // inline void bvc (const uint16_t address, registers& re, uint8_t pc_increaser)
        // {
        //     if (!(re.P & overflow_flag)) { branch_base (re); return; }
        //     increase_pc (pc_increaser);
        // }

        // inline void bvs (const uint16_t address, registers& re, uint8_t pc_increaser)
        // {
        //     if (re.P & overflow_flag) { branch_base (re); return; }
        //     increase_pc (pc_increaser);
        // }

        // inline void bcc (const uint16_t address, registers& re, uint8_t pc_increaser)
        // {
        //     if (!(re.P & carry_flag)) { branch_base (re); return; }
        //     increase_pc (pc_increaser);
        // }

        // inline void bcs (const uint16_t address, registers& re, uint8_t pc_increaser)
        // {
        //     if (re.P & carry_flag) { branch_base (re); return; }
        //     increase_pc (pc_increaser);
        // }

        // inline void bne (const uint16_t address, registers& re, uint8_t pc_increaser)
        // {
        //     if (!(re.P & zero_flag)) { branch_base (re); return; }
        //     increase_pc (pc_increaser);
        // }

        // inline void beq (const uint16_t address, registers& re, uint8_t pc_increaser)
        // {
        //     if (re.P & zero_flag) { branch_base (re); return; }
        //     increase_pc (pc_increaser);
        // }

        // /*
        // * I don't know why, but this was the hardest instruction to code. Maybe because
        // * my english skills aren't good enough.
        // */
        // inline void brk (const uint16_t address, registers& re, uint8_t pc_increaser)
        // {
        //     const uint16_t pc = re.PC + 2;
        //     push_stack (static_cast<uint8_t>(pc >> 8), re);
        //     push_stack (static_cast<uint8_t>(pc & 0xFF), re);
        //     push_stack (re.P, re);
        //     re.PC = static_cast<uint16_t>(read_memory (0xFFFE)) + (static_cast<uint16_t>(read_memory (0xFFFF)) << 8);
        //     re.P |= break_command_flag;
        //     std::cout<<"brk address: "<<re.PC<<"\n";
        // }

        // namespace mode
        // {
        //     // OK
        //     uint8_t accumulator (registers& re, const function_base& f)
        //     {
        //         f.base (max_value_16, re, 1);
        //         return f.cycles;
        //     }

        //     // OK
        //     uint8_t implied (registers& re, const function_base& f)
        //     {
        //         /*
        //         * I need to pass something.
        //         */
        //         f.base (re.A, re, 1);
        //         return f.cycles;
        //     }

        //     // OK
        //     uint8_t immediate (registers& re, const function_base& f)
        //     {
        //         // std::cout << "immediate mode\n";
        //         uint16_t address = re.PC + 1;
        //         f.base (address, re, 2);
        //         return f.cycles;
        //     }

        //     uint8_t indirect (registers& re, const function_base& f)
        //     {
        //         uint16_t address;
        //         get_address_indirect (address, re);
        //         f.base (address, re, 1);
        //         return f.cycles;
        //     }

        //     // OK
        //     uint8_t absolute (registers& re, const function_base& f)
        //     {
        //         uint16_t address;
        //         get_address_absolute (address, re);
        //         f.base (address, re, 3);
        //         return f.cycles;
        //     }

        //     // OK
        //     uint8_t zero_page (registers& re, const function_base& f)
        //     {
        //         uint16_t address;
        //         get_address_zero_page (address, re);
        //         f.base (address, re, 2);
        //         return f.cycles;
        //     }

        //     uint8_t relative (registers& re, const function_base& f)
        //     {
        //         uint16_t address;
        //         get_address_relative (address, re);
        //         f.base (address, re, 2);
        //         return f.cycles + get_extra_cycles_branch ();
        //     }

        //     // OK
        //     template<typename T>
        //     uint8_t absolute_indexed_x (registers& re, const function_base& f);

        //     // OK
        //     template<>
        //     uint8_t absolute_indexed_x<uint8_t> (registers& re, const function_base& f)
        //     {
        //         uint16_t address;
        //         const uint8_t c = get_address_absolute_indexed<uint8_t> (address, re, re.X);
        //         f.base (address, re, 3);
        //         return f.cycles + c;
        //     }

        //     // OK
        //     template<>
        //     uint8_t absolute_indexed_x<void> (registers& re, const function_base& f)
        //     {
        //         uint16_t address;
        //         get_address_absolute_indexed<void> (address, re, re.X);
        //         f.base (address, re, 3);
        //         return f.cycles;
        //     }

        //     // OK
        //     template<typename T>
        //     uint8_t absolute_indexed_y (registers& re, const function_base& f);

        //     // OK
        //     template<>
        //     uint8_t absolute_indexed_y<uint8_t> (registers& re, const function_base& f)
        //     {
        //         uint16_t address;
        //         const uint8_t c = get_address_absolute_indexed<uint8_t> (address, re, re.Y);
        //         f.base (address, re, 3);
        //         return f.cycles + c;
        //     }

        //     // OK
        //     template<>
        //     uint8_t absolute_indexed_y<void> (registers& re, const function_base& f)
        //     {
        //         uint16_t address;
        //         get_address_absolute_indexed<void> (address, re, re.Y);
        //         f.base (address, re, 3);
        //         return f.cycles;
        //     }

        //     // OK
        //     uint8_t zero_page_indexed_x (registers& re, const function_base& f)
        //     {
        //         uint16_t address;
        //         get_address_zero_page_indexed (address, re, re.X);
        //         f.base (address, re, 2);
        //         return f.cycles;
        //     }

        //     // OK
        //     uint8_t zero_page_indexed_y (registers& re, const function_base& f)
        //     {
        //         uint16_t address;
        //         get_address_zero_page_indexed (address, re, re.Y);
        //         f.base (address, re, 2);
        //         return f.cycles;
        //     }

        //     // OK
        //     uint8_t zero_page_indexed_indirect (registers& re, const function_base& f)
        //     {
        //         uint16_t address;
        //         get_address_zero_page_indexed_indirect (address, re);
        //         f.base (address, re, 2);
        //         return f.cycles;
        //     }

        //     template<typename T> uint8_t zero_page_indirect_indexed_y (registers& re, const function_base&);

        //     // OK
        //     template<>
        //     uint8_t zero_page_indirect_indexed_y<void> (registers& re, const function_base& f)
        //     {
        //         uint16_t address;
        //         get_address_zero_page_indirect_indexed_y (address, re);
        //         f.base (address, re, 2);
        //         return f.cycles;
        //     }

        //     // OK
        //     template<>
        //     uint8_t zero_page_indirect_indexed_y<uint8_t> (registers& re, const function_base& f)
        //     {
        //         uint16_t address;
        //         const uint8_t c = get_address_zero_page_indirect_indexed_y (address, re);
        //         f.base (address, re, 2);
        //         return f.cycles + c;
        //     }
        // }

        
        registers& get_registers ()
        {
            return r;
        }

        std::ostream& operator<< (std::ostream& os, const registers& re)
        {
            os<<"--registers--\n";
            os<<"PC: "<<(int)re.PC<<"\n";
            os<<"P: "<<std::bitset<8>(re.P)<<"\n";
            os<<"A: "<<(int)re.A<<"\n";
            os<<"X: "<<(int)re.X<<"\n";
            os<<"Y: "<<(int)re.Y<<"\n";
            os<<"SP: "<<(int)re.SP<<"\n";
            return os;
        }

        static uint16_t get_reset_vector ()
        {
            return static_cast<uint16_t>(read_memory (0xFFFC)) + (static_cast<uint16_t>(read_memory(0xFFFD)) << 8);
        }

        void turn_on ()
        {
            r = registers
            { 
                .PC = get_reset_vector (), 
                .SP = 0xFD, 
                .A = 0x0, 
                .X = 0x0, 
                .Y = 0x0, 
                .P = 0x34
            };

            // instructions = std::map<uint8_t, instruction>
            // {
            //     // ADC
            //     {0x69, instruction{"adc immediate",                     2, mode::immediate,                             function_base{two_cycles, adc}}},
            //     {0x65, instruction{"adc zero page",                     2, mode::zero_page,                             function_base{three_cycles, adc}}},
            //     {0x75, instruction{"adc zero page indexed x",           2, mode::zero_page_indexed_x,                   function_base{four_cycles, adc}}},
            //     {0x6D, instruction{"adc absolute",                      3, mode::absolute,                              function_base{four_cycles, adc}}},
            //     {0x7D, instruction{"adc absolute indexed x",            3, mode::absolute_indexed_x<uint8_t>,           function_base{four_cycles, adc}}},
            //     {0x79, instruction{"adc absolute indexed y",            3, mode::absolute_indexed_y<uint8_t>,           function_base{four_cycles, adc}}},
            //     {0x61, instruction{"adc zero page indexed indirect",    2, mode::zero_page_indexed_indirect,            function_base{six_cycles, adc}}},
            //     {0x71, instruction{"adc zero page indirect indexed y",  2, mode::zero_page_indirect_indexed_y<uint8_t>, function_base{five_cycles, adc}}},

            //     // SBC
            //     {0xE9, instruction{"sbc immediate",                     2, mode::immediate,                             function_base{two_cycles, sbc}}},
            //     {0xE5, instruction{"sbc zero page",                     2, mode::zero_page,                             function_base{three_cycles, sbc}}},
            //     {0xF5, instruction{"sbc zero page indexed x",           2, mode::zero_page_indexed_x,                   function_base{four_cycles, sbc}}},
            //     {0xED, instruction{"sbc absolute",                      3, mode::absolute,                              function_base{four_cycles, sbc}}},
            //     {0xFD, instruction{"sbc absolute indexed x",            3, mode::absolute_indexed_x<uint8_t>,           function_base{four_cycles, sbc}}},
            //     {0xF9, instruction{"sbc absolute indexed y",            3, mode::absolute_indexed_y<uint8_t>,           function_base{four_cycles, sbc}}},
            //     {0xE1, instruction{"sbc zero page indexed indirect",    2, mode::zero_page_indexed_indirect,            function_base{six_cycles, sbc}}},
            //     {0xF1, instruction{"sbc zero page indirect indexed y",  2, mode::zero_page_indirect_indexed_y<uint8_t>, function_base{five_cycles, sbc}}},

            //     // AND
            //     {0x29, instruction{"anda immediate",                     2, mode::immediate,                             function_base{two_cycles, anda}}},
            //     {0x25, instruction{"anda zero page",                     2, mode::zero_page,                             function_base{three_cycles, anda}}},
            //     {0x35, instruction{"anda zero page indexed x",           2, mode::zero_page_indexed_x,                   function_base{four_cycles, anda}}},
            //     {0x2D, instruction{"anda absolute",                      3, mode::absolute,                              function_base{four_cycles, anda}}},
            //     {0x3D, instruction{"anda absolute indexed x",            3, mode::absolute_indexed_x<uint8_t>,           function_base{four_cycles, anda}}},
            //     {0x39, instruction{"anda absolute indexed y",            3, mode::absolute_indexed_y<uint8_t>,           function_base{four_cycles, anda}}},
            //     {0x21, instruction{"anda zero page indexed indirect",    2, mode::zero_page_indexed_indirect,            function_base{six_cycles, anda}}},
            //     {0x31, instruction{"anda zero page indirect indexed y",  2, mode::zero_page_indirect_indexed_y<uint8_t>, function_base{five_cycles, anda}}},

            //     // OR
            //     {0x09, instruction{"ora immediate",                     2, mode::immediate,                             function_base{two_cycles, ora}}},
            //     {0x05, instruction{"ora zero page",                     2, mode::zero_page,                             function_base{three_cycles, ora}}},
            //     {0x15, instruction{"ora zero page indexed x",           2, mode::zero_page_indexed_x,                   function_base{four_cycles, ora}}},
            //     {0x0D, instruction{"ora absolute",                      3, mode::absolute,                              function_base{four_cycles, ora}}},
            //     {0x1D, instruction{"ora absolute indexed x",            3, mode::absolute_indexed_x<uint8_t>,           function_base{four_cycles, ora}}},
            //     {0x19, instruction{"ora absolute indexed y",            3, mode::absolute_indexed_y<uint8_t>,           function_base{four_cycles, ora}}},
            //     {0x01, instruction{"ora zero page indexed indirect",    2, mode::zero_page_indexed_indirect,            function_base{six_cycles, ora}}},
            //     {0x11, instruction{"ora zero page indirect indexed y",  2, mode::zero_page_indirect_indexed_y<uint8_t>, function_base{five_cycles, ora}}},

            //     // EOR
            //     {0x49, instruction{"eor immediate",                     2, mode::immediate,                             function_base{two_cycles, eor}}},
            //     {0x45, instruction{"eor zero page",                     2, mode::zero_page,                             function_base{three_cycles, eor}}},
            //     {0x55, instruction{"eor zero page indexed x",           2, mode::zero_page_indexed_x,                   function_base{four_cycles, eor}}},
            //     {0x4D, instruction{"eor absolute",                      3, mode::absolute,                              function_base{four_cycles, eor}}},
            //     {0x5D, instruction{"eor absolute indexed x",            3, mode::absolute_indexed_x<uint8_t>,           function_base{four_cycles, eor}}},
            //     {0x59, instruction{"eor absolute indexed y",            3, mode::absolute_indexed_y<uint8_t>,           function_base{four_cycles, eor}}},
            //     {0x41, instruction{"eor zero page indexed indirect",    2, mode::zero_page_indexed_indirect,            function_base{six_cycles, eor}}},
            //     {0x51, instruction{"eor zero page indirect indexed y",  2, mode::zero_page_indirect_indexed_y<uint8_t>, function_base{five_cycles, eor}}},

            //     // LDA
            //     {0xA9, instruction{"lda immediate",                     2, mode::immediate,                             function_base{two_cycles, lda}}},
            //     {0xA5, instruction{"lda zero page",                     2, mode::zero_page,                             function_base{three_cycles, lda}}},
            //     {0xB5, instruction{"lda zero page indexed x",           2, mode::zero_page_indexed_x,                   function_base{four_cycles, lda}}},
            //     {0xAD, instruction{"lda absolute",                      3, mode::absolute,                              function_base{four_cycles, lda}}},
            //     {0xBD, instruction{"lda absolute indexed x",            3, mode::absolute_indexed_x<uint8_t>,           function_base{four_cycles, lda}}},
            //     {0xB9, instruction{"lda absolute indexed y",            3, mode::absolute_indexed_y<uint8_t>,           function_base{four_cycles, lda}}},
            //     {0xA1, instruction{"lda zero page indexed indirect",    2, mode::zero_page_indexed_indirect,            function_base{six_cycles, lda}}},
            //     {0xB1, instruction{"lda zero page indirect indexed y",  2, mode::zero_page_indirect_indexed_y<uint8_t>, function_base{five_cycles, lda}}},

            //     // LDX
            //     {0xA2, instruction{"ldx immediate",                     2, mode::immediate,                             function_base{two_cycles, ldx}}},
            //     {0xA6, instruction{"ldx zero page",                     2, mode::zero_page,                             function_base{three_cycles, ldx}}},
            //     {0xB6, instruction{"ldx zero page indexed y",           2, mode::zero_page_indexed_y,                   function_base{four_cycles, ldx}}},
            //     {0xAE, instruction{"ldx absolute",                      3, mode::absolute,                              function_base{four_cycles, ldx}}},
            //     {0xBE, instruction{"ldx absolute indexed y",            3, mode::absolute_indexed_y<uint8_t>,           function_base{four_cycles, ldx}}},

            //     // LDY
            //     {0xA0, instruction{"ldy immediate",                     2, mode::immediate,                             function_base{two_cycles, ldy}}},
            //     {0xA4, instruction{"ldy zero page",                     2, mode::zero_page,                             function_base{three_cycles, ldy}}},
            //     {0xB4, instruction{"ldy zero page indexed x",           2, mode::zero_page_indexed_x,                   function_base{four_cycles, ldy}}},
            //     {0xAC, instruction{"ldy absolute",                      3, mode::absolute,                              function_base{four_cycles, ldy}}},
            //     {0xBC, instruction{"ldy absolute indexed x",            3, mode::absolute_indexed_x<uint8_t>,           function_base{four_cycles, ldy}}},

            //     // ASL
            //     {0x0A, instruction{"asl accumulator",                   1, mode::accumulator,                           function_base{two_cycles, asl}}},
            //     {0x06, instruction{"asl zero page",                     2, mode::zero_page,                             function_base{five_cycles, asl}}},
            //     {0x16, instruction{"asl zero page indexed x",           2, mode::zero_page_indexed_x,                   function_base{six_cycles, asl}}},
            //     {0x0E, instruction{"asl absolute",                      3, mode::absolute,                              function_base{six_cycles, asl}}},
            //     {0x1E, instruction{"asl absolute indexed x",            3, mode::absolute_indexed_x<void>,              function_base{seven_cycles, asl}}},

            //     // INC
            //     {0xE6, instruction{"inc zero page",                     2, mode::zero_page,                             function_base{five_cycles, inc}}},
            //     {0xF6, instruction{"inc zero page indexed x",           2, mode::zero_page_indexed_x,                   function_base{six_cycles, inc}}},
            //     {0xEE, instruction{"inc absolute",                      3, mode::absolute,                              function_base{six_cycles, inc}}},
            //     {0xFE, instruction{"inc absolute indexed x",            3, mode::absolute_indexed_x<void>,              function_base{seven_cycles, inc}}},

            //     // DEC
            //     {0xC6, instruction{"dec zero page",                     2, mode::zero_page,                             function_base{five_cycles, dec}}},
            //     {0xD6, instruction{"dec zero page indexed x",           2, mode::zero_page_indexed_x,                   function_base{six_cycles, dec}}},
            //     {0xCE, instruction{"dec absolute",                      3, mode::absolute,                              function_base{six_cycles, dec}}},
            //     {0xDE, instruction{"dec absolute indexed x",            3, mode::absolute_indexed_x<void>,              function_base{seven_cycles, dec}}},

            //     // ROR
            //     {0x6A, instruction{"ror accumulator",                   1, mode::accumulator,                           function_base{two_cycles, ror}}},
            //     {0x66, instruction{"ror zero page",                     2, mode::zero_page,                             function_base{five_cycles, ror}}},
            //     {0x76, instruction{"ror zero page indexed x",           2, mode::zero_page_indexed_x,                   function_base{six_cycles, ror}}},
            //     {0x6E, instruction{"ror absolute",                      3, mode::absolute,                              function_base{six_cycles, ror}}},
            //     {0x7E, instruction{"ror absolute indexed x",            3, mode::absolute_indexed_x<void>,              function_base{seven_cycles, ror}}},

            //     // ROL
            //     {0x2A, instruction{"rol accumulator",                   1, mode::accumulator,                           function_base{two_cycles, rol}}},
            //     {0x26, instruction{"rol zero page",                     2, mode::zero_page,                             function_base{five_cycles, rol}}},
            //     {0x36, instruction{"rol zero page indexed x",           2, mode::zero_page_indexed_x,                   function_base{six_cycles, rol}}},
            //     {0x2E, instruction{"rol absolute",                      3, mode::absolute,                              function_base{six_cycles, rol}}},
            //     {0x3E, instruction{"rol absolute indexed x",            3, mode::absolute_indexed_x<void>,              function_base{seven_cycles, rol}}},

            //     // CLC
            //     {0x18, instruction{"clc implied",                       1, mode::implied,                               function_base{two_cycles, clc}}},

            //     // SEC
            //     {0x38, instruction{"sec implied",                       1, mode::implied,                               function_base{two_cycles, sec}}},

            //     // CLI
            //     {0x58, instruction{"cli implied",                       1, mode::implied,                               function_base{two_cycles, cli}}},

            //     // SEI
            //     {0x78, instruction{"sei implied",                       1, mode::implied,                               function_base{two_cycles, sei}}},

            //     // CLV
            //     {0xB8, instruction{"clv implied",                       1, mode::implied,                               function_base{two_cycles, clv}}},

            //     // CLD
            //     {0xD8, instruction{"cld implied",                       1, mode::implied,                               function_base{two_cycles, cld}}},

            //     // SED
            //     {0xF8, instruction{"sed implied",                       1, mode::implied,                               function_base{two_cycles, sed}}},

            //     // NOP
            //     {0xEA, instruction{"nop implied",                       1, mode::implied,                               function_base{two_cycles, sed}}},

            //     // BIT
            //     {0x46, instruction{"bit zero page",                     2, mode::zero_page,                             function_base{three_cycles, bit}}},
            //     {0x4E, instruction{"bit absolute",                      3, mode::absolute,                              function_base{four_cycles, bit}}},

            //     // STA
            //     {0x85, instruction{"sta zero page",                     2, mode::zero_page,                             function_base{three_cycles, sta}}},
            //     {0x95, instruction{"sta zero page indexed x",           2, mode::zero_page_indexed_x,                   function_base{four_cycles, sta}}},
            //     {0x8D, instruction{"sta absolute",                      3, mode::absolute,                              function_base{four_cycles, sta}}},
            //     {0x9D, instruction{"sta absolute indexed x",            3, mode::absolute_indexed_x<void>,              function_base{five_cycles, sta}}},
            //     {0x99, instruction{"sta absolute indexed y",            3, mode::absolute_indexed_y<void>,              function_base{five_cycles, sta}}},
            //     {0x81, instruction{"sta zero page indexed indirect",    2, mode::zero_page_indexed_indirect,            function_base{six_cycles, sta}}},
            //     {0x91, instruction{"sta zero page indirect indexed y",  2, mode::zero_page_indirect_indexed_y<void>,    function_base{six_cycles, sta}}},

            //     // STX
            //     {0x86, instruction{"stx zero page",                     2, mode::zero_page,                             function_base{three_cycles, stx}}},
            //     {0x96, instruction{"stx zero page indexed y",           2, mode::zero_page_indexed_y,                   function_base{four_cycles, stx}}},
            //     {0x8E, instruction{"stx absolute",                      3, mode::absolute,                              function_base{four_cycles, stx}}},

            //     // STY
            //     {0x84, instruction{"sty zero page",                     2, mode::zero_page,                             function_base{three_cycles, sty}}},
            //     {0x94, instruction{"sty zero page indexed y",           2, mode::zero_page_indexed_y,                   function_base{four_cycles, sty}}},
            //     {0x8C, instruction{"sty absolute",                      3, mode::absolute,                              function_base{four_cycles, sty}}},

            //     // JMP
            //     {0x4C, instruction{"jmp absolute",                      3, mode::absolute,                              function_base{three_cycles, jmp<void>}}},
            //     {0x6C, instruction{"jmp indirect",                      3, mode::indirect,                              function_base{five_cycles, jmp<uint8_t>}}},

            //     // CMP
            //     {0xC9, instruction{"cmp immediate",                     2, mode::immediate,                             function_base{two_cycles, cmp}}},
            //     {0xC5, instruction{"cmp zero page",                     2, mode::zero_page,                             function_base{three_cycles, cmp}}},
            //     {0xD5, instruction{"cmp zero page indexed x",           2, mode::zero_page_indexed_x,                   function_base{four_cycles, cmp}}},
            //     {0xCD, instruction{"cmp absolute",                      3, mode::absolute,                              function_base{four_cycles, cmp}}},
            //     {0xDD, instruction{"cmp absolute indexed x",            3, mode::absolute_indexed_x<uint8_t>,           function_base{four_cycles, cmp}}},
            //     {0xD9, instruction{"cmp absolute indexed y",            3, mode::absolute_indexed_y<uint8_t>,           function_base{four_cycles, cmp}}},
            //     {0xC1, instruction{"cmp zero page indexed indirect",    2, mode::zero_page_indexed_indirect,            function_base{six_cycles, cmp}}},
            //     {0xD1, instruction{"cmp zero page indirect indexed y",  2, mode::zero_page_indirect_indexed_y<uint8_t>, function_base{five_cycles, cmp}}},

            //     // CPX
            //     {0xE0, instruction{"cpx immediate",                     2, mode::immediate,                             function_base{two_cycles, cpx}}},
            //     {0xE4, instruction{"cpx zero page",                     2, mode::zero_page,                             function_base{three_cycles, cpx}}},
            //     {0xEC, instruction{"cpx absolute",                      3, mode::absolute,                              function_base{four_cycles, cpx}}},

            //     // CPX
            //     {0xC0, instruction{"cpy immediate",                     2, mode::immediate,                             function_base{two_cycles, cpy}}},
            //     {0xC4, instruction{"cpy zero page",                     2, mode::zero_page,                             function_base{three_cycles, cpy}}},
            //     {0xCC, instruction{"cpy absolute",                      3, mode::absolute,                              function_base{four_cycles, cpy}}},

            //     // TXS
            //     {0x9A, instruction{"txs implied",                       1, mode::implied,                               function_base{two_cycles, txs}}},

            //     // TSX
            //     {0xBA, instruction{"tsx implied",                       1, mode::implied,                               function_base{two_cycles, tsx}}},

            //     // PHA
            //     {0x48, instruction{"pha implied",                       1, mode::implied,                               function_base{three_cycles, pha}}},

            //     // PLA
            //     {0x68, instruction{"pla implied",                       1, mode::implied,                               function_base{four_cycles, pla}}},

            //     // PHP
            //     {0x08, instruction{"php implied",                       1, mode::implied,                               function_base{three_cycles, php}}},

            //     // PLP
            //     {0x28, instruction{"plp implied",                       1, mode::implied,                               function_base{four_cycles, plp}}},

            //     // TAX
            //     {0xAA, instruction{"tax implied",                       1, mode::implied,                               function_base{two_cycles, tax}}},

            //     // TXA
            //     {0x8A, instruction{"txa implied",                       1, mode::implied,                               function_base{two_cycles, txa}}},

            //     // DEX
            //     {0xCA, instruction{"dex implied",                       1, mode::implied,                               function_base{two_cycles, dex}}},

            //     // INX
            //     {0xE8, instruction{"inx implied",                       1, mode::implied,                               function_base{two_cycles, inx}}},

            //     // TAY
            //     {0xA8, instruction{"tay implied",                       1, mode::implied,                               function_base{two_cycles, tay}}},

            //     // TYA
            //     {0x98, instruction{"tya implied",                       1, mode::implied,                               function_base{two_cycles, tya}}},

            //     // DEY
            //     {0x88, instruction{"dey implied",                       1, mode::implied,                               function_base{two_cycles, dey}}},

            //     // INY
            //     {0xC8, instruction{"iny implied",                       1, mode::implied,                               function_base{two_cycles, iny}}},

            //     // LSR
            //     {0x4A, instruction{"lsr accumulator",                   1, mode::accumulator,                           function_base{two_cycles, lsr}}},
            //     {0x46, instruction{"lsr zero page",                     2, mode::zero_page,                             function_base{five_cycles, lsr}}},
            //     {0x56, instruction{"lsr zero page indexed x",           2, mode::zero_page_indexed_x,                   function_base{six_cycles, lsr}}},
            //     {0x4E, instruction{"lsr absolute",                      3, mode::absolute,                              function_base{six_cycles, lsr}}},
            //     {0x5E, instruction{"lsr absolute indexed x",            3, mode::absolute_indexed_x<void>,              function_base{seven_cycles, lsr}}},

            //     // RTS
            //     {0x60, instruction{"rts implied",                       1, mode::implied,                               function_base{six_cycles, rts}}},

            //     // RTI
            //     {0x40, instruction{"rti implied",                       1, mode::implied,                               function_base{six_cycles, rti}}},

            //     // JSR
            //     {0x20, instruction{"jsr absolute",                      3, mode::absolute,                              function_base{six_cycles, jsr}}},

            //     // BPL
            //     {0x10, instruction{"bpl relative",                      2, mode::relative,                              function_base{two_cycles, bpl}}},

            //     // BMI
            //     {0x30, instruction{"bmi relative",                      2, mode::relative,                              function_base{two_cycles, bmi}}},

            //     // BVC
            //     {0x50, instruction{"bvc relative",                      2, mode::relative,                              function_base{two_cycles, bvc}}},

            //     // BVS
            //     {0x70, instruction{"bvs relative",                      2, mode::relative,                              function_base{two_cycles, bvs}}},

            //     // BCC
            //     {0x90, instruction{"bcc relative",                      2, mode::relative,                              function_base{two_cycles, bcc}}},

            //     // BCS
            //     {0xB0, instruction{"bcs relative",                      2, mode::relative,                              function_base{two_cycles, bcs}}},

            //     // BNE
            //     {0xD0, instruction{"bne relative",                      2, mode::relative,                              function_base{two_cycles, bne}}},

            //     // BEQ
            //     {0xF0, instruction{"beq relative",                      2, mode::relative,                              function_base{two_cycles, beq}}},

            //     //BRK
            //     {0x00, instruction{"brk implied",                       1, mode::implied,                               function_base{seven_cycles, brk}}},
            // };

            // r = registers{0x0000, 0xFD, 0x00, 0x00, 0x00, break_command_flag};
        }

        uint8_t cycles = 0;

        /*
        * This method returns remaining cycles.
        */
        long long unsigned int counter = 0;
        const uint8_t tick ()
        {
            // if (!cycles)
            // {
            //     // std::cout<<"PC: "<<r.PC<<"\n";
            //     uint8_t i = jnes::cpu::memory_map[r.PC];
            //     std::cout<<"opcode: "<<(int)i<<", data: "<<(int)jnes::cpu::memory_map[r.PC + 1]<<", data 2: "<<(int)jnes::cpu::memory_map[r.PC + 2]<<"\n";
            //     // // std::cout<<"address: "<<(uint16_t)read_memory(r.PC + 1)<<((uint16_t)read_memory(r.PC + 2) << 8)<<"\n";
            //     if (instructions.find (i) != instructions.end ())
            //     {
            //         // std::cout << "-------------------instruction: " << counter++ << "-----------------\n";
            //         std::cout<<"Trying to exec: "<<instructions[i].name<<"\n";
            //         cycles = instructions[i].func(r, instructions[i].fb);
            //         // std::cout << "--New values--\n";
            //         // std::cout<<r<<"\n";
            //     }
            //     else
            //     {
            //         std::cout << "Total pushes: " << push_counter << "\nTotal pulles: " << pull_counter << "\n";
            //         debug::save_cpu_memory_map_in_error ();
            //         std::exit(-1);
            //     }
            // }
            // --cycles;
            // return cycles;
        }


        uint16_t cpu_wraparound (const uint16_t address)
        {
            if (address >= 0x0800 && address < 0x2000)
            {
                return address % 0x0800;
            }
            if (address >= 0x2008 && address < 0x3FFF)
            {
                return (address % 0x0008) + 0x2000;
            }

            return address;
        }

        void write_memory (uint16_t address, const uint8_t data)
        {
            address = cpu_wraparound (address);
            jnes::cpu::memory_map[address] = data;
            switch (static_cast<io_port>(address))
            {
                case io_port::ppu_control:
                    jnes::ppu::write_control (data);
                    break;
                case io_port::ppu_mask:
                    jnes::ppu::write_mask (data);
                    break;
                case io_port::sprite_dma_register:
                    jnes::ppu::write_dma (data);
                    break;
                case io_port::ppu_oam_address:
                    jnes::ppu::write_oam_address (data);
                case io_port::ppu_oam_data:
                    jnes::ppu::write_oam_data (data);
                default:
                    break;
            };
            
        }

        const uint8_t read_memory (uint16_t address)
        {
            // std::cout<<"old address: "<<address<<"\n";
            address = cpu_wraparound (address);
            // std::cout<<"new address: "<<address<<"\n";
            return jnes::cpu::memory_map[address];
        }

        void print_program ()
        {
            for (int address = 0x8000; address < 0xC000; address++)
                std::cout<<(int)read_memory(address);
        }

        void print_map ()
        {
            std::stringstream ss{std::string()};

            ss.flags (std::ios::hex);
            ss << "\t\t0F\t0E\t0D\t0C\t0B\t0A\t09\t08\t07\t06\t05\t04\t03\t02\t01\t00\n";

            for (int index = 0xFFFF; index > 0x0;)
            {
                int i = index - 0xF;

                if (index <= 0xF)           ss << "0x000" << i << "\t\t";
                else if (index <= 0xFF)     ss << "0x00" << i << "\t\t";
                else if (index <= 0xFFF)    ss << "0x0" << i << "\t\t";
                else                        ss << "0x" << i << "\t\t";

                for (int col = 0x0; col <= 0xF; ++col, --index)
                {
                    ss << "0x" << (int)memory_map[index] << "\t";
                }
                ss << "\n";
            }

            std::cout << ss.str();
        }

        void turn_off ()
        {
        }

    }
}