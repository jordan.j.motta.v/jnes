#include <window/jnes_window.h>

#define JNES_SDL
#ifdef JNES_SDL
#include <window/jnes_window_sdl.h>
static jnes::window::handler (*get_handler)() = jnes::window::sdl::get_handler;
#endif



namespace jnes
{
    namespace window
    {

        const uint8_t open ()
        {
            get_handler().open ();
        }

        bool is_open ()
        {
            return get_handler().is_open ();
        }

        void close ()
        {
            get_handler().close ();
        }

        void render ()
        {
            get_handler().render ();
        }
    }
}