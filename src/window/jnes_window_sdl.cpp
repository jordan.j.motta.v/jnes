#include <iostream>
#include <SDL2/SDL.h>
#include <window/jnes_window_sdl.h>

namespace jnes
{
    namespace window
    {
        namespace sdl
        {
            SDL_Window *_window;
            SDL_Renderer *_renderer;
            SDL_Event e;
            bool is_running = false;

            const uint8_t open ()
            {
                if (SDL_Init (SDL_INIT_VIDEO) != 0)
                {
                    std::cout<<"SDL Init video Error\n";
                    return -1;
                }
                
                _window = SDL_CreateWindow ("jnes", 100, 100, 320, 240, SDL_WINDOW_SHOWN);
                if (_window == nullptr)
                {
                    std::cout<<"Windows Creation failed!\n";
                    SDL_Quit ();
                    return -2;
                }

                _renderer = SDL_CreateRenderer (_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
                if (_renderer == nullptr)
                {
                    std::cout<<"Renderer Creation failed!\n";
                    SDL_DestroyWindow (_window);
                    SDL_Quit ();
                    return -3;
                }
                is_running = true;
                // std::cout<<"open window. is_running: "<<is_running<<"\n";
                return 0;
            }

            void render ()
            {

            }

            bool is_open ()
            {
                SDL_PollEvent (&e);
                if (e.type == SDL_QUIT)
                {
                    std::cout<<"Exit!\n";
                    is_running = false;
                }
                // std::cout<<"is_open.is_running: "<<is_running<<"\n";
                return is_running;
            }

            void close ()
            {
                SDL_DestroyWindow (_window);
                SDL_DestroyRenderer (_renderer);
                SDL_Quit ();
            }

            jnes::window::handler get_handler ()
            {
                return jnes::window::handler{open, is_open, close, render};
            }
        }
    }
}