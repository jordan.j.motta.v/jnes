#include <iostream>
#include <array>
#include <limits>
#include <mmc/jnes_mmc.h>
#include "jnes_nrom.h"


const uint8_t trainer_presence = 0b00000100;

namespace jnes
{
    namespace cpu
    {
        extern std::array<uint8_t, 0x10000> memory_map;
    }

    namespace ppu
    {
        extern std::array<uint8_t, 16 * 1024> memory_map;
    }
}

struct
{
    void (*_load_cpu)(std::array<uint8_t, 0x10000>&, const std::vector<uint8_t>&, const uint8_t);
    void (*_load_ppu)(std::array<uint8_t, 16 * 1024>&, const std::vector<uint8_t>&, const uint8_t);
    void (*_swap_cpu_bank)(std::array<uint8_t, 0x10000>&, const std::vector<uint8_t>&, const uint8_t);
} _mmc;

const jnes::system::ines *program;

enum class mmc_type : uint8_t
{
    nrom = 0,
    mmc1,
    unrom,
    cnrom,
    mmc3,
    mmc5,
    ffe_f4xxx,
    aorom,
    ffe_f3xxx,
    mmc2,
    mmc4,
    color_dreams,
    ffe_f6xxx,
    _100_in_1 = 15,
    bandai_chip,
    ffe_f8xxx,
    jaleco_ss8806,
    namcot_106,
    nintendo_disk_system,
    konami_vrc4a,
    konami_vrc4a2,
    konami_vrc6,
    konami_vrc4b,
    irem_g_101 = 32,
    taito_tc0190_tc0350,
    _32kb_rom,
    tengen_rambo_1 = 64,
    irem_h_3001,
    gnrom,
    sun_soft3,
    sun_soft4,
    sun_soft5_fme_7,
    camerica = 71,
    irem_74hc161_32_based = 78,
    pirate_hk_sf3 = 91
};

static void set_nrom ()
{
    _mmc._load_cpu = jnes::mmc::nrom::load_cpu;
    _mmc._load_ppu = jnes::mmc::nrom::load_ppu;
    _mmc._swap_cpu_bank = jnes::mmc::nrom::swap_cpu_bank;
}

void jnes::mmc::set_mmc (const jnes::system::ines *i)
{
    program = i;
    const uint8_t mmc_code = i->header.rom_control_1 | (i->header.rom_control_2 << 4);
    switch (static_cast<mmc_type>(mmc_code))
    {
        case mmc_type::nrom:
            std::cout<<"nrom type\n";
            set_nrom ();
            break;
    }
}

void jnes::mmc::load_banks ()
{
    std::cout<<"starting load banks\n";
    const uint8_t header_size = 0x10;
    const uint16_t program_block_size = std::numeric_limits<uint8_t>::max ();
    uint16_t trainer = 0;
    if (program->header.rom_control_1 & trainer_presence) trainer = 0x200; // 512 bytes
    _mmc._load_cpu (jnes::cpu::memory_map, program->data, trainer + header_size);
    _mmc._load_ppu (jnes::ppu::memory_map, program->data, trainer + header_size + (program->header.program_rom_blocks * program_block_size));
    std::cout<<"banks loaded\n";
}