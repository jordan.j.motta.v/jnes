#include <array>
#define JNES_DEBUG
#ifdef JNES_DEBUG
#include <iostream>
#endif
#include <jnes_commons.h>
#include <mmc/jnes_nrom.h>
#include <jnes_cpu.h>
#include <jnes_ppu.h>


using namespace std;

void jnes::mmc::nrom::load_cpu (std::array<uint8_t, 0x10000>& memory_map, const std::vector<uint8_t>& data, const uint8_t index)
{
    std::cout<<"filling cpu memory\n";
    jnes::commons::fill_memory (memory_map, 0x8000, data, index, 0x4000);
    jnes::commons::fill_memory (memory_map, 0xC000, data, index, 0x4000);
    jnes::commons::fill_memory (memory_map, 0x6000, data, index, 0x2000);
//     for (uint16_t address = 0x8000, i = 0; address < 0xC000; address += 16, i += 16)
//    {
//        /*
//        * I need to implement a generic filler that will save me from write this for each mmc type.
//        */
//        memory_map[address] = data[index + i];
//        memory_map[address + 1] = data[index + i + 1];
//        memory_map[address + 2] = data[index + i + 2];
//        memory_map[address + 3] = data[index + i + 3];
//        memory_map[address + 4] = data[index + i + 4];
//        memory_map[address + 5] = data[index + i + 5];
//        memory_map[address + 6] = data[index + i + 6];
//        memory_map[address + 7] = data[index + i + 7];
//        memory_map[address + 8] = data[index + i + 8];
//        memory_map[address + 9] = data[index + i + 9];
//        memory_map[address + 10] = data[index + i + 10];
//        memory_map[address + 11] = data[index + i + 11];
//        memory_map[address + 12] = data[index + i + 12];
//        memory_map[address + 13] = data[index + i + 13];
//        memory_map[address + 14] = data[index + i + 14];
//        memory_map[address + 15] = data[index + i + 15];

//         // Second block
//        memory_map[0x4000 + address] = data[index + i];
//        memory_map[0x4000 + address + 1] = data[index + i + 1];
//        memory_map[0x4000 + address + 2] = data[index + i + 2];
//        memory_map[0x4000 + address + 3] = data[index + i + 3];
//        memory_map[0x4000 + address + 4] = data[index + i + 4];
//        memory_map[0x4000 + address + 5] = data[index + i + 5];
//        memory_map[0x4000 + address + 6] = data[index + i + 6];
//        memory_map[0x4000 + address + 7] = data[index + i + 7];
//        memory_map[0x4000 + address + 8] = data[index + i + 8];
//        memory_map[0x4000 + address + 9] = data[index + i + 9];
//        memory_map[0x4000 + address + 10] = data[index + i + 10];
//        memory_map[0x4000 + address + 11] = data[index + i + 11];
//        memory_map[0x4000 + address + 12] = data[index + i + 12];
//        memory_map[0x4000 + address + 13] = data[index + i + 13];
//        memory_map[0x4000 + address + 14] = data[index + i + 14];
//        memory_map[0x4000 + address + 15] = data[index + i + 15];
//    }
}

void jnes::mmc::nrom::load_ppu (std::array<uint8_t, 16 * 1024>& memory_map, const std::vector<uint8_t>& data, const uint8_t index)
{
//     std::cout<<"filling ppu memory\n";
//      for (uint16_t address = 0x0, i = 0; address < 0x2000; address += 16, i += 16)
//    {
//        memory_map[address] = data[index + i];
//        memory_map[address + 1] = data[index + i + 1];
//        memory_map[address + 2] = data[index + i + 2];
//        memory_map[address + 3] = data[index + i + 3];
//        memory_map[address + 4] = data[index + i + 4];
//        memory_map[address + 5] = data[index + i + 5];
//        memory_map[address + 6] = data[index + i + 6];
//        memory_map[address + 7] = data[index + i + 7];
//        memory_map[address + 8] = data[index + i + 8];
//        memory_map[address + 9] = data[index + i + 9];
//        memory_map[address + 10] = data[index + i + 10];
//        memory_map[address + 11] = data[index + i + 11];
//        memory_map[address + 12] = data[index + i + 12];
//        memory_map[address + 13] = data[index + i + 13];
//        memory_map[address + 14] = data[index + i + 14];
//        memory_map[address + 15] = data[index + i + 15];
//    }
}

void jnes::mmc::nrom::swap_cpu_bank (std::array<uint8_t, 0x10000>&, const std::vector<uint8_t>& data, const uint8_t index)
{
    
}