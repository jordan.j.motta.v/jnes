#include <utility>
#include <map>
#include <jnes_cpu_instructions.hpp>


using namespace jnes::cpu;
using  address_and_cycles = std::pair<uint16_t /* address */, uint8_t /* extra cycles */>;


inline const address_and_cycles get_address_zero_page (const registers& re, const uint8_t)
{
    return std::make_pair(static_cast<uint16_t>(read_memory(re.PC + 1)), 0);
}

inline const address_and_cycles get_address_zero_page_indexed (const registers& re, const uint8_t index)
{
    return std::make_pair(static_cast<uint16_t>(read_memory (re.PC + 1 + index)), 0);
}

inline const address_and_cycles get_address_absolute (const registers& re, const uint8_t)
{
	const uint16_t address = static_cast<uint16_t>(read_memory (re.PC + 1)) + (static_cast<uint16_t>(read_memory (re.PC + 2)) << 8);
    return std::make_pair(address, 0);
}

inline const address_and_cycles get_address_absolute_indexed (const registers& re, const uint8_t indexer)
{
	/* BUG 
	* the cpu will never get an extra cycles because the lsb type is 16 bits.
	*/
    const uint16_t lsb = static_cast<uint16_t>(read_memory(re.PC + 1));
    const uint16_t msb = static_cast<uint16_t>(read_memory(re.PC + 2)) << 8;
    const uint16_t address = lsb + msb + indexer;
    return lsb + indexer < indexer ? std::make_pair(address, 1) : std::make_pair(address, 0);
}

inline address_and_cycles get_address_indirect (const registers& re, const uint8_t)
{
    const uint16_t address1 = static_cast<uint16_t>(read_memory(re.PC)) + 
                (static_cast<uint16_t>(read_memory(re.PC + 1)) << 8);

    const uint16_t address2 = static_cast<uint16_t>(read_memory(address1)) + 
                (static_cast<uint16_t>(read_memory(address1 + 1)) << 8);

    return std::make_pair(address2, 0);
}

inline address_and_cycles get_address_indexed_indirect (const registers& re, const uint8_t)
{
    const uint16_t index = read_memory(re.PC + 1) + re.X;
    const uint16_t lsb = static_cast<uint16_t>(read_memory(index));
    const uint16_t msb = static_cast<uint16_t>(read_memory(index + 1)) << 8;

    return std::make_pair (lsb + msb, 0);
}

inline address_and_cycles get_address_indirect_indexed (const registers& re, const uint8_t)
{
    const uint8_t index = read_memory (re.PC + 1);
    const uint8_t lsb = read_memory(index);
    const uint16_t msb = static_cast<uint16_t>(read_memory(index + 1)) << 8;


    const uint16_t address = static_cast<uint16_t>(lsb) + msb + static_cast<uint16_t>(re.Y);

    // if (lsb + re.Y < re.Y) return one_cycle;
    // else return zero;
    return lsb + re.Y < re.Y ? std::make_pair(address, 1) : std::make_pair(address, 0);
}

/*
* std::pair<uint8_t value to operate, uint8_t extra_cycles>
*/
const std::pair<uint8_t, uint8_t> get_value_mode (const registers& re, mode mode)
{
	std::pair<uint8_t, uint8_t> values;

	auto proccess_address = [&re] (auto func, const uint8_t index) -> std::pair<uint8_t, uint8_t>
	{
		address_and_cycles av = func (re, index);
		return std::make_pair (read_memory(av.first), av.second);
	};

	switch (static_cast<int>(mode))
	{
		case static_cast<int>(mode::accumulator):
			values = std::make_pair (re.A, 0);
			break;
		case static_cast<int>(mode::implied):
			//
			break;
		case static_cast<int>(mode::immediate):
			values = std::make_pair (read_memory(re.PC + 1), 0);
			break;
		case static_cast<int>(mode::indirect):
			values = proccess_address (get_address_indirect, 0);
			break;
		case static_cast<int>(mode::absolute):
			values = proccess_address(get_address_absolute, 0);
			break;
		case static_cast<int>(mode::zero_page):
			values = proccess_address(get_address_zero_page, 0);
			break;
		case static_cast<int>(mode::relative):
			//
			break;
		case static_cast<int>(mode::absolute_indexed_x):
			values = proccess_address(get_address_absolute_indexed, re.X);
			break;
		case static_cast<int>(mode::absolute_indexed_y):
			values = proccess_address(get_address_absolute_indexed, re.Y);
			break;
		case static_cast<int>(mode::zero_page_indexed_x):
			values = proccess_address (get_address_zero_page_indexed, re.X);
			break;
		case static_cast<int>(mode::zero_page_indexed_y):
			values = proccess_address (get_address_zero_page_indexed, re.Y);
			break;
		case static_cast<int>(mode::zero_page_indexed_indirect):
			values = proccess_address (get_address_indexed_indirect, 0);
			break;
		case static_cast<int>(mode::zero_page_indirect_indexed_y):
			values = proccess_address (get_address_indirect_indexed, 0);
			break;
	}
	return values;
}

std::map<const uint8_t, instruction> instructions
{
	
};













instruction& fetch_instruction (const uint8_t code)
{
	return instructions[code];
}