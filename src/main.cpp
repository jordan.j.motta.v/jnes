// #include <jnes.h>
#include <iostream>
#include <vector>
#include <string>
#include <tests/jnes_tests.hpp>

// using namespace jnes;

constexpr char all_tests[] = "-ta";
constexpr char cpu_tests[] = "-tc";

bool has_test_param ()
{
	return false;
}

void print_help ()
{
	std::cout << "JNES. A nes emulator\n";
	std::cout<< "Basic command: jnes <game file>\n\n";

	std::cout << "-t\t Run all tests\n";
	std::cout << "-tc\t Run cpu tests\n";
	std::cout << "-h\t Print help\n";
}

std::vector<std::string> extract_params (int size, char *params[])
{
	std::vector<std::string> ps;

	for (int n = 0; n < size; ++n)
	{
		ps.push_back (std::string{params[n]});
	}

	return ps;
}

int main (int argv, char *argc[])
{

	if (argv == 1)
	{
		print_help ();
	}

	for (int n = 1; n < argv; ++n)
	{
		const std::string param{argc[n]};
		
		if (param == "-ta")
		{
			std::cout << "Test every component of JNES!\n";

		}
		else if ( param == "-tc" )
		{
			std::cout << "Test CPU of JNES!\n";
			jnes::tests::test_cpu();
		}
	}


    // system::turn_on ();
    // system::load_game ("../dk.nes");
    // system::run_game ();
    // system::turn_off ();
}