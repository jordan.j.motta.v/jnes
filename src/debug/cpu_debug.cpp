#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstdint>
#include <cpu_debug.h>
#include <jnes_cpu.h>

namespace jnes
{
    namespace cpu
    {
        extern std::array<uint8_t, 0x10000> memory_map;
    }

    namespace debug
    {
        static std::size_t instructions = 0;
        void instructions_limit ()
        {
            if (!instructions)
            {
                std::cout  << "Number of instructions to execute: ";
                std::cin >> instructions;
            }
        }

        static std::string to_hex_string (const int n)
        {
            std::stringstream ss;
            ss.flags (std::ios::hex);
            ss << "0x";
            if (n < 0x10) ss << "0";
            ss << n;
            return ss.str();
        }

        static std::string append_instructions (const int p_address)
        {
            std::string s;
            for (int i = 0x0; i < 0x10; ++i)
            {
                s.append (to_hex_string(cpu::memory_map[p_address - i]));
                s.append (", ");
            }
            return s;
        }

        void save_cpu_memory_map_in_error ()
        {
            std::ofstream out;
            out.open ("memory_map_in_error.log");
            if (out.is_open ())
            {
                std::stringstream ss;
                ss.flags (std::ios::hex);
                const std::string title("--------------An error ocurred---------------\n\n\n");
                out.write (title.c_str (), title.size ());
                std::string subtitle = "----------CPU registers in error----------\n";
                ss << subtitle;
                cpu::registers re = cpu::get_registers ();
                ss << "PC: " << (int)re.PC << "\n";
                ss << "A: " << (int)re.A << "\n";
                ss << "X: " << (int)re.X << "\n";
                ss << "Y: " << (int)re.Y << "\n";
                ss << "SP: " << (int)re.SP << "\n";
                ss << "P: " << (int)re.P << "\n\n\n";
                ss << "---------------cpu memory map--------------\n\n";
                out.write (ss.str ().c_str (), ss.str ().size ());
                for (int address = 0xFFFF; address > 0; address -= 0x10)
                {
                    ss.str(std::string());
                    ss << "0x";
                    if ( address < 0x10)
                    {
                        ss << "000";
                    }
                    else if ( address < 0x100)
                    {
                        ss << "00";
                    }
                    else if (address < 0x1000)
                    {
                        ss << "0";
                    }
                    ss << address;
                    std::string s = append_instructions (address);
                    std::string line = ss.str() + ": " + s + "\n";
                    out.write (line.c_str (), line.size ());
                    // std::cout << ss.str () << ": " << s << "\n";
                }
                out.close ();
            }
        }
    }
}