#include <array>
#include <iostream>
#include <jnes_ppu.h>

namespace jnes
{
    namespace cpu
    {
        extern std::array<uint8_t, 0x10000> memory_map;
        extern void nmi_interruption ();
    }

    namespace ppu
    {

        constexpr uint8_t vblack_process        = 0b10000000;
        constexpr uint8_t sprite_0_bit_flag     = 0b01000000;
        constexpr uint8_t scanline_sprite_count = 0b00100000;
        constexpr uint8_t vram_write_ignorer    = 0b00010000;
        constexpr uint8_t emphasize_red         = 0b10000000;
        constexpr uint8_t emphasize_green       = 0b01000000;
        constexpr uint8_t emphasize_blue        = 0b00100000;
        constexpr uint8_t show_sprites          = 0b00010000;
        constexpr uint8_t show_background       = 0b00001000;
        constexpr uint8_t show_sprites_left     = 0b00000100; // Long name. I cut it.
        constexpr uint8_t show_background_left  = 0b00000010; // Long name. I cut it.
        constexpr uint8_t greyscale             = 0b00000001; 

        std::array<uint8_t, 16 * 1024> memory_map;

        /*
        * control and mask variable are optionals, i think. I can handle those variables
        * through cpu memory_map.
        */
        static uint8_t control;
        static uint8_t mask;
        static uint16_t scroll;
        static uint16_t oam_address;
        static uint16_t address_register;
        static uint8_t status;

        void turn_on ()
        {
            
        }


        uint16_t wraparound (uint16_t address)
        {
            if (address > 0x4000)
            {
                address = address % 0x4000;
            }
            if (address >= 0x3F20 && address < 0x4000)
            {
                return (address % 0x20) + 0x3000;
            }
            if (address >= 0x3000 && address < 0x3F00)
            {
                return address % 0x3000;
            }
        }

        void write_control (const uint8_t data)
        {
            control = cpu::memory_map[0x2000];
        }

        void write_mask (const uint8_t data)
        {
            mask = cpu::memory_map[0x2001];
        }

        void write_oam_address (const uint8_t data)
        {
            oam_address <<= 8;
            oam_address |= static_cast<uint16_t>(data);
        }

        void write_oam_data (const uint8_t data)
        {
            // Nothing to do here.
        }

        void write_address (const uint8_t data)
        {
            address_register = (address_register << 8) | static_cast<uint16_t>(data);
        }

        void write_dma (const uint8_t data)
        {
            const uint16_t cpu_address = static_cast<uint16_t>(data) << 8;
            for (auto i = 0; i < 0xFF; ++i, ++oam_address)
            {
                memory_map[oam_address] = cpu::memory_map[cpu_address + i];
            }
        }

        inline void set_vblack ()
        {
            status |= vblack_process;
            cpu::memory_map[0x2002] = status;
            if (control & vblack_process)
            {
                jnes::cpu::nmi_interruption ();
            }
            std::cout<<"vblack ocurring\n";
        }

        static inline void clear_vblack ()
        {
            status &= ~vblack_process;
            cpu::memory_map[0x2002] = status;
            // std::cout<<"vblack clear\n";
        }

        static void idle_cycle ()
        {

        }

        static void fecth_tile ()
        {

        }

        static void fecth_next_tile ()
        {

        }

        static void fetch_tile_shift_register ()
        {

        }

        static void fetch_two_bytes ()
        {

        }

        /*
        * I have no idea what to do here XD. I must read more about ppu.
        */
        int16_t scanline = -1;
        uint16_t cycle = 0;
        void exec ()
        {
            constexpr int ppu_cycles_per_cpu_cycle = 3;
            /*
            * For each cpu cycle. PPU do 3 cycles.
            */
           for (int c = 0; c < ppu_cycles_per_cpu_cycle; ++c, ++cycle)
           {

               if (scanline >= 0 && scanline < 240)
               {
                   if (cycle == 0)
                   {
                       idle_cycle ();
                   }
                   else if (cycle > 0 && cycle <= 256)
                   {
                       fecth_tile ();
                   }
                   else if (cycle > 256 && cycle <= 320)
                   {
                       fecth_next_tile ();
                   }
                   else if (cycle > 320 && cycle <= 336)
                   {
                       fetch_tile_shift_register ();
                   }
                   else if (cycle > 336 && cycle <= 340)
                   {
                       fetch_two_bytes ();
                   }
               }
               else if (scanline == 241 && cycle == 0) set_vblack ();
               if (cycle > 340)
               {
                //    std::cout<<"scanline: " << scanline << "\n";
                   cycle = -1;
                   ++scanline;
                   if (scanline > 260)
                   {
                       clear_vblack ();
                       scanline = -1;
                       std::cout<<"vblack clear\n";
                   }
               }
           }
        }

        void turn_off ()
        {

        }
    }
}





