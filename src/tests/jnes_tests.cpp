#include <iostream>
#include <jnes_cpu.h>
#include <array>

#include <cassert>
#include <tests/jnes_tests.hpp>
#include <tests/jnes_color.hpp>
#include <jnes_cpu_instructions.hpp>


using namespace jnes::colors;
using std::cout;

namespace jnes
{
	namespace cpu
	{
		extern std::array<uint8_t, 0x10000> memory_map;
		extern void print_map ();
	}
}

void test_turn_on ()
{
	cout << make_message("Testing turn_on cpu method", title) << "\n";
	jnes::cpu::turn_on ();
	jnes::cpu::registers re = jnes::cpu::get_registers ();
	if (re.PC == 0xFDFC && re.SP == 0xFD && re.X == 0x00 && re.Y == 0x00 && re.A == 0x00 && re.P == 0x34)
	{
		cout << make_message("success", success) << "\n";
	}
	else
	{
		cout << make_message("Something went wrong with the registers.\n", error) << "\n";
		cout << "Registers\n";
		cout << re << "\n";
	}
}

void jnes::tests::test_cpu ()
{
	jnes::cpu::registers re = jnes::cpu::get_registers ();
	// cout << make_message("Prueba", title) << "\n";

	cout << "Filling cpu memory\n";
	for (int index = 0; index < 0x10000; ++index)
	{
		jnes::cpu::memory_map[index] = (uint8_t)index;
	}

	jnes::cpu::print_map ();
	// return;
	
	test_turn_on ();



	cout << make_message("Testing write, read methods", title) << "\n";
	jnes::cpu::write_memory (0x500, 40);

	int value = jnes::cpu::read_memory (0x500);
	// assert (value == 40);
	if (value == 40)
	{
		cout << make_message("write_memory success", success) << "\n";
	}
	else 
	{
		cout << make_message("Error", error) << "\n";
		cout << "You read value: " << value << ", but you expected: " << 40 << "\n";
		std::exit (-1);
	}

	cout << make_message("Testing push stack, pull stack methods", title) << "\n";
	jnes::cpu::push_stack (30);
	value = jnes::cpu::pull_stack ();
	if (value == 30)
	{
		cout << make_message("push_stack success", success) << "\n";
	}
	else 
	{
		cout << make_message("Error", error) << "\n";
		cout << "You read value: " << value << ", but you expected: " << 40 << "\n";
		std::exit (-1);
	}
}

void jnes::tests::test_all ()
{

}
