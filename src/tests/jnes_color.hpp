#ifndef JNES_COLOR_HPP
#define JNES_COLOR_HPP

#include <string>

namespace jnes
{
	namespace colors
	{
		const std::string title = "\033[1;34m\033[0m";
		const std::string error = "\033[1;31m\033[0m";
		const std::string success = "\033[1;32m\033[0m";

		const std::string make_message(const char* const message, std::string color)
		{
			color.insert(7, message);
			return color;
		}
	}
}

#endif /* jnes_color.hpp */