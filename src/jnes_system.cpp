#include <iostream>
#include <vector>
#include <thread>
#include <chrono>
#include <cstdint>
#include <jnes_system.h>
#include <jnes_cpu.h>
#include <jnes_ppu.h>
#include <mmc/jnes_mmc.h>
#include <window/jnes_window.h>
#include <debug.h>

#define JNES_DEBUG

using namespace std;

extern array<uint8_t, 0x10000> memory_map;
extern jnes::cpu::registers r;

constexpr uint8_t mirroring_type        = 0b00000001;
constexpr uint8_t battery_backed        = 0b00000010;
constexpr uint8_t trainer_presence      = 0b00000100;
constexpr uint8_t four_screen_mirroring = 0b00001000;
constexpr uint8_t mmc_lower             = 0b11110000;
constexpr uint8_t mmc_higher            = 0b11110000;

/*
* Modern cpu speed surpass the 2C02.
* I think i need to emulate this. I guess.
*/
constexpr uint64_t delay ()
{
    return static_cast<uint64_t>(236.25f / 11.0f);
}


jnes::system::ines game;

void jnes::system::turn_on ()
{
    std::cout.flags (ios::hex | ios::showbase);
    // jnes::window::turn_on ();
    jnes::cpu::turn_on ();
    jnes::ppu::turn_on ();
    jnes::window::open ();
}

void read_ines (fstream& game_data)
{
    game_data.seekg (0, ios::end);
    int length = game_data.tellg();
    game_data.seekg (0, ios::beg);
    jnes::debug::printg (length);
    char trom[length];
    game_data.read (trom, length);
    game.data = vector<uint8_t>(length);
    game.length = length;
    game.data.assign (trom, trom + length);
    game.header.program_rom_blocks = game.data[4];
    game.header.video_rom_blocks = game.data[5];
    game.header.rom_control_1 = game.data[6];
    game.header.rom_control_2 = game.data[7];
    game.header.ram_blocks = game.data[8];
    game.header.flag_9 = game.data[9];
}

ostream& jnes::system::operator<< (std::ostream& os, const ines& i)
{
    os<<"--ines structure--\n";
    os<<"rom size:"<<i.data.size ()<<"\n\n";
    os<<"--header\n";
    os<<"program_blocks: "<<(int)i.header.program_rom_blocks<<"\n";
    os<<"video rom blocks: "<<(int)i.header.video_rom_blocks<<"\n";
    os<<"-rom control\n";
    os<<"mirroring type: "<<(int)(i.header.rom_control_1 & mirroring_type)<<"\n";
    os<<"battery backed: "<<(int)(i.header.rom_control_1 & battery_backed)<<"\n";
    os<<"trainer presence: "<<(int)(i.header.rom_control_1 & trainer_presence)<<"\n";
    os<<"four screen mirroring: "<<(int)(i.header.rom_control_1 & four_screen_mirroring)<<"\n";
    os<<"mmc lower: "<<(int)(i.header.rom_control_1 & mmc_lower)<<"\n";
    os<<"mmc higher: "<<(int)(i.header.rom_control_2 & mmc_higher)<<"\n";
    return os;
}

jnes::errors::error_type jnes::system::load_game (const string& path)
{
    fstream reader;
    reader.open (path, ios::in);
    if (!reader.is_open ())
    {
        reader.close ();
        errors::open_file (path);
        return errors::error_type::file_no_found;
    }

    read_ines (reader);
    std::cout<<game<<"\n";
    jnes::mmc::set_mmc (&game);
    jnes::mmc::load_banks ();
    // jnes::cpu::set_program_counter ();
    std::cout<<"program loaded\n";

    return jnes::errors::error_type::no_problem;
}

jnes::errors::error_type jnes::system::run_game ()
{
    // std::cout<<"is_open: "<<jnes::window::is_open ()<<"\n";
    
    while (jnes::window::is_open ())
    {
        jnes::cpu::tick();
        jnes::ppu::exec ();
        jnes::window::render ();
        std::this_thread::sleep_for (std::chrono::microseconds (delay ()));
    }

    jnes::cpu::turn_off ();
    jnes::ppu::turn_off ();
    jnes::window::close ();

    return errors::error_type::no_problem;
}

void jnes::system::turn_off ()
{
    
}