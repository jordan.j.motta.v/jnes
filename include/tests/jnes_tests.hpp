#ifndef JNES_TESTS_HPP
#define JNES_TESTS_HPP

namespace jnes
{
	namespace tests
	{
		void test_cpu ();
		void test_all ();
	}
}

#endif /* jnes_tests.hpp */