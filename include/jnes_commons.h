/*
* Here we can find some functions that don't
* belong to any component of the NES system.
*/

#include <cstdint>

#ifndef JNES_COMMONS_H
#define JNES_COMMONS_H

namespace jnes
{
    namespace commons
    {
        template <typename T, typename U>
        void fill_memory (T& memory, const uint16_t memory_index_start, U& data, const uint16_t data_index_start, const uint16_t chunk_data_size)
        {
            uint16_t i = memory_index_start;
            uint16_t j = data_index_start;
            for (int n = 0; n < chunk_data_size; ++n, ++i, ++j)
            {
                memory[i] = data[j];
            }
        }
    }
}

#endif /* jnes_commons.h */