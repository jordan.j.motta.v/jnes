#ifndef JNES_CPU_INSTRUCTIONS_HPP
#define JNES_CPU_INSTRUCTIONS_HPP

#include <cstdint>
#include <string>

#include "jnes_cpu.h"

namespace jnes
{
	namespace cpu
	{

			enum class mode
			{
				accumulator,
				implied,
				immediate,
				indirect,
				absolute,
				zero_page,
				relative,
				absolute_indexed_x,
				absolute_indexed_y,
				zero_page_indexed_x,
				zero_page_indexed_y,
				zero_page_indexed_indirect,
				zero_page_indirect_indexed_y
			};

			struct instruction
			{
				std::string name;
				std::uint8_t length;
				std::uint8_t cycles;
				mode m;
				void (*func)(jnes::cpu::registers&, mode);
			};

			const instruction& fetch_instruction (const uint8_t code);
	}
}

#endif /* jnes_cpu_instructions.hpp */