#ifndef JNES_PPU_H
#define JNES_PPU_H

namespace jnes
{
    /*
    * The ppu in the nes is an 2C02 processor, and it contains the graphics
    * to be rendered on the game.
    * 
    * The ppu can address 16bit(wich means 64kb theoretically), but only 16kb of RAM are
    * available. Due to this difference, any address between 0x4000-0xFFFF is wrapped around,
    * it makes a mirror of 0x0000-0x3FFF.
    * 
    * REGISTERS
    * 
    * The cpu read from and write to PPU using the registers located at 0x2006 and 0x2007. This
    * should be do during the V-BLACK, because this may affect the address used while drawing
    * and corrupt what is displayed.
    * 
    * To write any data to ppu, the address must be specified, but the ppu address are 16bit and
    * I/O registers are 8bit. So the cpu must write two times in 0x2006, an then read/write in
    * 0x2007. If the cpu write in 0x2007, the address is incremented by 1(horizontal) or 32(vertical).
    * The first read from 0x2007 is invalid, and the data will be automatically buffered, waiting
    * for the next read. This doesn't apply in colour palettes.
    */
    namespace ppu
    {
        void turn_on ();
        void exec ();
        void write_control (const uint8_t); // 0x2000
        void write_mask (const uint8_t); // 0x2001
        void write_oam_address (const uint8_t); // 0x2003
        void write_oam_data (const uint8_t); // 0x2004
        void write_scroll (const uint8_t); // 0x2005
        void write_address (const uint8_t); // 0x2006
        void write_data (const uint8_t); // 0x2007
        void write_dma (const uint8_t); // 0x4014
        void turn_off ();
    }
}

#endif /* jnes_ppu.h */