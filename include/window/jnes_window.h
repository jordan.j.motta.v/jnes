#include <cstdint>
#ifndef JNES_WINDOW
#define JNES_WINDOW

namespace jnes
{
    namespace window
    {
        struct handler
        {
            const uint8_t (*open)();
            bool (*is_open)();
            void (*close)();
            void (*render)();
        };

        const uint8_t open ();
        bool is_open ();
        void render ();
        void close ();
    }
}

#endif /* jnes_window.h */