#include "jnes_window.h"
#ifndef JNES_WINDOW_SDL_H
#define JNES_WINDOW_SDL_H

namespace jnes
{
    namespace window
    {
        namespace sdl
        {
            jnes::window::handler get_handler ();
        }
    }
}

#endif /* jnes_window_sdl.h */