#include <fstream>
#include <string>
#include <array>
#include <vector>
// #include "jnes_cpu.h"
#include "jnes_errors.h"
#include <ostream>

#ifndef JNES_SYSTEM_H
#define JNES_SYSTEM_H

namespace jnes
{
    namespace system
    {

        struct ines
        {
            /*
            * This contains file data;
            */
            std::vector<uint8_t> data;
            /*
            * Innecesary variable. std::vector::size resolve this problem.
            */
            int length;

            struct
            {
                /*
                * NES 
                */
                uint8_t nes[3];

                /*
                * 16384 * program_rom_block = 16kb * program_rom_block
                */
                uint8_t program_rom_blocks;

                /*
                * 8192 * video_rom_blocks = 8kb * video_rom_block
                */
                uint8_t video_rom_blocks;

                /*
                * rom_control_1 flags
                * 
                * bit 0 -> it inidicates the mirroring type.
                *           0 = horizontal mirroring.
                *           1 = vertical mirroring.
                * 
                * bit 1 -> indicates the presence of battery-backed RAM
                *           at memory locations 0x6000-0x7FFF
                * 
                * bit 2 -> inidicates the presences of the trainer (512 bytes)
                *           at 0x7000-71FF
                * 
                * bit 3 -> If this byte is set, it overrides bit 0 to indicate
                *           four screen mirroring should be used
                * 
                * bit 4-7 -> four lower bits of the mapper number
                */
                uint8_t rom_control_1;

                /*
                * bit 0-3 -> reserved
                * 
                * bit 4-7 -> four upper bits of the mapper number
                */
                uint8_t rom_control_2;

                /*
                * 8kb ram blocks numbers.
                * 8192 * ram_blocks = 8kb * ram_blocks
                * If ram_block is 0. assume 1.
                */
                uint8_t ram_blocks;
                uint8_t flag_9; //reserved
            } header;
        };

        std::ostream& operator<< (std::ostream&, const ines&);

        void turn_on ();

        /*
        * return some value lesser than 0, in order to indicate an error has ocurred.
        * return 0. OK
        */
        errors::error_type load_game (const std::string& path);
        errors::error_type run_game ();

        void turn_off ();
    }
}

#endif /* jnes_system.h */