#ifndef CPU_DEBUG_H
#define CPU_DEBUG_H
#include <array>
#include <string>

namespace jnes
{
    namespace debug
    {
        void instructions_limit ();
        void save_cpu_memory_map ();
        void save_cpu_memory_map_in_error ();
        void push_stack_info (const std::string& i, const uint8_t data );
        
    }
}

#endif /* cpu_debug.h */