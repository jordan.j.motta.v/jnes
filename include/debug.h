#include <vector>

#ifndef DEBUG_H
#define DEBUG_H

namespace jnes
{
    namespace debug
    {

        template <typename T>
        void print (const std::vector<T>& v)
        {
            for (auto d : v)
            {
                std::cout<<"data: "<<d<<" \n";
            }
        }

        template <typename T>
        void printg (const T& v)
        {
            std::cout<<"value length: "<<v<<"\n";
        }
    }
}

#endif /* debug.h */