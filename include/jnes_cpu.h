#include <ostream>
#include <string>
#include <cstdint>
#include "jnes_cpu_instructions.hpp"
#ifndef JNES_CPU_H
#define JNES_CPU_H

namespace jnes
{
	/*
	 * The cpu of nes system is a specialization of 6502 processors
	 * called 2A03. This chip doesn't support graphics, so it was
	 * neccesary to use another chip called PPU (Picture Processor Unit)
	 * */
	namespace cpu
	{

		/*
		 * See a struct's name in plural make noise to me.
		 * 
		 * But i think this is the best name.
		*/
		struct registers
		{
			/*
			 * PC -> Program Counter
			 * 16 bits pointer that point to the next instruction. The value of the next
			 * instruction can be affected by breaks, jumps, procedure calls and interruptions
			 * */
			uint16_t PC;

			/*
			 * SP -> Stack Pointer
			 * Located at memory locations 0x0100-0x01FF.
			 * It works as top-down. So when you push on to the stack, the stack pointer
			 * decrements, and when you pull from the stack, the stack pointer increments.
			 * 
			 * There is no detection of stack overflow. This mean the stack pointer will wrap
			 * around 0x00-0xFF 
			*/
			uint8_t SP;

			/*
			 * A -> Accumulator
			 * 
			 * 8-bits register wich stores arithmetics and logics results.
			 * It can be used to set value from memory too.
			*/
			uint8_t A;

			/*
			 * X -> Index register
			 * 
			 * 8-bits register used as counter tipically, or used as an offset for certain
			 * addressing mode. It can be used to set a value retrieved from memory and to
			 * get or set  value of the stack pointer.
			*/
			uint8_t X;

			/*
			 * Y -> Index register
			 * 
			 * 8-bits register similar to X register. But it cannot affect SP.
			*/
			uint8_t Y;

			/*
			 * P -> Processor status
			 * 
			 * 8-bits register which contains flags that are setted or cleared when
			 * an instruction is executed.
			 * 
			 * Those flags are:
			 * 
			 * 1) C (Carry flag): This flag is set when after a arithmetic operation
			 * 					perform a overflow (greater that 255) or an underflow
			 * 					(Lesser than 0)
			 * 
			 * 2) Z (Zero Flag): It is set when the last arithmetic operation result
			 * 					in 0.
			 * 
			 * 3) I (Interrupt Disable): It can be used to prevent the system responding
			 * 					to IRQs.
			 * 
			 * 4) D (Decimal Mode): It is used to switch the 6205 processor into BCD mode.
			 * 					The nes system use 2A03 doesn't support BCD mode, this flag
			 * 					is ignored.
			 * 
			 * 5) B (Break Command): It is used to indicate that a BRK (Break) intruction
			 * 					has been executed, causing a IRQ.
			 * 
			 * 6) V (Overflow Flag): This flag is used when an invalid two's complements
			 * 					was obtained by the previous instruction. This means that a
			 * 					negative result has been obtained when a positive result was
			 * 					expected and vice versa.
			 * 
			 * 7) N (Negative Flag): Negative flag represent the sign of the byte during
			 * 					during the operation.
			 * 
			 * Representation.
			 * 
			 * 
			 * 		________________________________
			 * 		| N | V |  | B | D | I | Z | C |
			 * 		|___|___|__|___|___|___|___|___|
			 * 		  7   6      4   3   2   1   0
			*/
			uint8_t P;
		};

		std::ostream& operator<< (std::ostream&, const registers&);

		// struct instruction
		// {
		// 	/*
		// 	* DEBUG
		// 	*/
		// 	std::string name;

		// 	/*
		// 	* Valid values: 1, 2, 3
		// 	*/
		// 	uint8_t length;

		// 	 cycles 
		// 	uint8_t cycles;

		// 	cpu_instructions::mode mode;

		// 	uint8_t (*func)(registers&, const cpu_instructions::mode);
		// };

		registers& get_registers ();

		/*
		* Prepare all variables to emulate the game.
		*/
		void turn_on ();

		const uint8_t tick ();

		void write_memory (const uint16_t address, uint8_t data);

		const uint8_t read_memory (const uint16_t address);

		void set_program_counter ();

		void push_stack (const uint8_t);

		uint8_t pull_stack ();

		void increase_pc (const uint8_t);

		void print_program ();

		void turn_off ();

	}
}

#endif	/* jnes_cpu.h */
