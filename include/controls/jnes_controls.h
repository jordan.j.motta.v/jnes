#include <cstdint>
#ifndef JNES_CONTROL
#define JNES_CONTROL

namespace jnes
{
    namespace controls
    {
        enum struct key
        {
            up,
            left,
            right,
            down,
            a,
            b,
            start,
            select
        };

        const uint8_t get_key ();
        const bool is_key_pressed (key);
    }
}

#endif /* jnes_control.h */