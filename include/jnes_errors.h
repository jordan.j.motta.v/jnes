#include <iostream>
#include <string>

#ifndef JNES_ERRORS_H
#define JNES_ERRORS_H

namespace jnes
{
    namespace errors
    {
        enum class error_type {
            file_no_found,
            no_problem
        };

        void open_file (const std::string& path);
    }
}

#endif /* jnes_errors.h */