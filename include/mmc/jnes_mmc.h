#include <jnes_system.h>

#ifndef JNES_MMC_H
#define JNES_MMC_H

namespace jnes
{
    namespace mmc
    {
        void set_mmc (const jnes::system::ines*);
        void load_banks ();
    }
}

#endif /* jnes_mmc.h */