#include <cstdint>
#include <vector>


/*
* This mmc type assign the first prg-block to 0x8000 and the second to 0xC000.
* The switching is only allowed to 0x8000 block; 0xC000 block is assigned permantely.
* NROM doesn't support vrom, games using it have 8KB of VRAM at 0x0000 in PPU memory.
*/
#ifndef JNES_NROM_H
#define JNES_NROM_H

namespace jnes
{
    namespace mmc
    {
        namespace nrom
        {
            void load_cpu (std::array<uint8_t, 0x10000>&, const std::vector<uint8_t>&, const uint8_t);
            void load_ppu (std::array<uint8_t, 16 * 1024>&, const std::vector<uint8_t>&, const uint8_t);
            void swap_cpu_bank (std::array<uint8_t, 0x10000>&, const std::vector<uint8_t>&, const uint8_t);
        }
    }
}

#endif /* jnes_nrom.h */